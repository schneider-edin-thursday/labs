#include "../inc/tree.h"
#include <iostream>

namespace lab7 {
    void clear(node *to_clear);

    // Construct an empty tree
    tree::tree() {
        root = nullptr;
        tree_size = 0;
    }

    //Copy helper
    node* copyHelper(const node* other){

        if(other == nullptr){
            return nullptr;
        }

        node *new_node = new node(other->data);

        new_node->left = copyHelper(other->left);
        new_node->right = copyHelper(other->right);

        return new_node;
    }


    // Copy constructor
    tree::tree(const tree &copy) {
        tree_size = copy.tree_size;
        root = copyHelper(copy.root);
    }

    // Deconstruct tree
    tree::~tree() {
        clear(root);
    }

    // Insert Helper
    void insert_helper(node* &root, int value){

        if(root == nullptr){
            node *new_node = new node(value);
            root = new_node;
            return;
        }
        if(value < root->data){
            insert_helper(root->left, value);
        }
        else if(value > root->data){
            insert_helper(root->right, value);
        }
        else{
            root->frequency++;
            return;
        }
    }

    // Insert
    void tree::insert(int value) {
        insert_helper(root, value);
        tree_size++;
    }

    // Remove key return true if the key is deleted, and false if it isn't in the tree
    bool tree::remove(int key) {
        if(in_tree(key)){
            node *q = nullptr;
            node *s, *t, *v = nullptr;
            node *r = new node(key);
            node *p = root;

            while(r->data != p->data){
                q = p;
                if(r->data < p->data)
                    p = p->left;
                if(r->data > p->data)
                    p = p->right;
            }

            if(p->frequency > 1){
                p->frequency--;
                tree_size--;
                return true;
            }

            if(p->left == nullptr)
                v = p->right;
            else if(p->right == nullptr)
                v = p->left;
            else{
                t = p;
                v = p->right;
                s = v->left;

                while(s != nullptr){
                    t = v;
                    v = s;
                    s = v->left;
                }

                if(t != p){
                    t->left = v->right;
                    v->right = p->right;
                }
                v->left = p->left;
            }

            if(q == nullptr)
                root = v;
            else if(p == q->left)
                q->left = v;
            else
                q->right = v;
            tree_size--;
            return true;
        }

        return false;
    }

    //Level Helper
    void level_helper(node *p, int key, int &counter){

        if(key < p->data){
            counter++;
            level_helper(p->left, key, counter);
        }
        else if(key > p->data){
            counter++;
            level_helper(p->right, key, counter);
        }
        else
            return;

    }

    // What level is key on?
    int tree::level(int key) {
        if(!in_tree(key))
            return -1;

        int counter = 0;
        node *p = root;
        level_helper(p, key, counter);

        return counter;
    }

    //path_to Helper
    void path_to_helper(node *p, int key){

        if(key < p->data){
            std::cout << p->data << " -> ";
            path_to_helper(p->left, key);
        }
        else if(key > p->data){
            std::cout << p->data << " -> ";
            path_to_helper(p->right, key);
        }
        else{
            std::cout << p->data << "\n";
            return;
        }
    }

    // Print the path to the key, starting with root
    void tree::path_to(int key) {
        if(!in_tree(key)){
            std::cout << "";
            return;
        }
        node *p = root;

        path_to_helper(p, key);
    }

    // Number of items in the tree
    unsigned tree::size() {
        return tree_size;
    }

    // Depth Helper
    int depth_helper(node *p){
        if(p == nullptr)
            return 0;
        else{
            int left_depth = depth_helper(p->left);
            int right_depth = depth_helper(p->right);

            if(left_depth > right_depth)
                return left_depth + 1;
            else
                return right_depth + 1;
        }
    }
    // Calculate the depth of the tree, longest string of connections
    unsigned tree::depth() {
        node *p = root;
        if(p == nullptr)
            return 0;
        else if(p->left == nullptr && p->right == nullptr)
            return 0;
        else
            return depth_helper(p) - 1;

    }

    // in_tree Helper
    bool in_tree_helper(node *p, int key){

        if(p == nullptr){
            return false;
        }
        if(key < p->data){
            return in_tree_helper(p->left, key);
        }
        else if(key > p->data){
            return in_tree_helper(p->right, key);
        }
        else{
            return true;
        }
    }

    // Determine whether the given key is in the tree
    bool tree::in_tree(int key) {
        node *p = root;
        return in_tree_helper(p, key);
    }

    // get_frequency Helper
    int get_freq_helper(node *p, int key){

        if(key < p->data){
            return get_freq_helper(p->left, key);
        }
        else if(key > p->data){
            return get_freq_helper(p->right, key);
        }
        else{
            return p->frequency;
        }
    }

    // Return the number of times that value is in the tree
    int tree::get_frequency(int key) {
        if(!in_tree(key))
            return 0;

        node *p = root;
        return get_freq_helper(p, key);
    }

    // to_string Helper
    std::string to_string_helper(node *p, std::string &s){

        if(p != nullptr){
            to_string_helper(p->left, s);
            for(int i = 0; i < p->frequency; i++){
                s += std::to_string(p->data);
                s += " ";
            }
            to_string_helper(p->right, s);
        }
        return s;
    }

    // Return a string of all of the elements in the tree in order
    std::string tree::to_string() {
        node *p = root;
        std::string s;
        s = to_string_helper(p, s);
        s += "\n";
        return s;
    }

    //Use the to string function for the following two functions
    // Print the tree least to greatest, Include duplicates
    void tree::print() {
        std::cout << to_string();
    }

    // Print the tree least to greatest, Include duplicates
    std::ostream &operator<<(std::ostream &stream, tree &RHS) {
        //print();
        return stream;
    }

    // Operator= Overload. Allowing for copying of trees
    tree &tree::operator=(const tree &rhs) {
        tree new_tree = tree(rhs);
        this->root = new_tree.root;
    }

    /**************************
     * Extra credit functions *
     **************************/

    // Return a vector with all of the nodes that are greater than the input key in the tree
    std::vector<int> tree::values_above(int key) {

    }

    // Merge rhs into this. Demo to a TA for credit
    tree tree::operator+(const tree &rhs) const {

    }

    // Balance the tree using any published algorithm. Demo to a TA for credit
    void tree::balance() {

    }

    /*********************************************************************
     * Recursion Example                                                 *
     * Print the linked list from greatest to least using recursion      *
     *********************************************************************/

    // Auxiliary functions
    void node_print_gtl(node *top) {
        if (top == nullptr) return;
        node_print_gtl(top->right);
        for (int i = 0; i < top->frequency; i++) std::cout << top->data << " ";
        node_print_gtl(top->left);
    }

    void clear(node *to_clear) {
        if (to_clear == nullptr) return;
        if (to_clear->left != nullptr) clear(to_clear->left);
        if (to_clear->right != nullptr) clear(to_clear->right);
        delete to_clear;
    }

    // Class function
    void tree::print_gtl() {
        node_print_gtl(root);
        std::cout << std::endl;
    }
}
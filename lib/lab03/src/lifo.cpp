
#include "lifo.h"

namespace lab3{
    lifo::lifo() {
    //Reserve 100 spaces in lifo_storage
        lifo_storage.reserve(100);
        index = 0;
    }

    lifo::lifo(std::string input_string) {
        lifo_storage.reserve(100);
        index = 0;
        push(input_string);
    }

    lifo::lifo(const lifo &original) {
        this->lifo_storage.operator=(original.lifo_storage);
        this->index = original.index;
    }

    lifo::~lifo() {
        lifo_storage.~stringVector();
    }

    lifo &lifo::operator=(const lifo &right) {
        this->lifo_storage.operator=(right.lifo_storage);
        this->index = right.index;
        return *this;
        //return <#initializer#>;
    }

    bool lifo::is_empty(){
        if(index == 0) {
            return true;
        }
        else {
            return false;
        }
    }

    int lifo::size(){
        return index;
    }

    std::string lifo::top(){
        if(is_empty()) {
            throw 1;
        }
        else {
            return lifo_storage[index-1];
        }
        //return std::__cxx11::string();
    }

    void lifo::push(std::string input) {
        lifo_storage.append(input);
        index++;
    }

    void lifo::pop() {
        if(is_empty()) {
            throw 1;
        }
        else {
            index--;
            lifo_storage.pop_back();
        }
    }
}

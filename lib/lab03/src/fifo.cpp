
#include "fifo.h"

namespace lab3{
    fifo::fifo() { //Default constructor. Set index properly and reserve 100 spaces in fifo_storage
    //Reserve 100 spaces in fifo_storage
        fifo_storage.reserve(100);
        front_index = 0;
        back_index = 0;
    }

    fifo::fifo(std::string input_string) { //Constructor that does the same thing as the default constructor,
        fifo_storage.reserve(100);           //but adds a single item to the Fifo
        front_index = 0;
        back_index = 0;
        enqueue(input_string);
    }

    fifo::fifo(const fifo &original) { //copy consturctor, use this to implement '-' operator
        this->fifo_storage.operator=(original.fifo_storage);
        this->front_index = original.front_index;
        this->back_index = original.back_index;
    }

    fifo::~fifo() { //Destructor
        fifo_storage.~stringVector();
    }

    fifo &fifo::operator=(const fifo &right) { //Assignment operator
        this->fifo_storage.operator=(right.fifo_storage);
        this->front_index = right.front_index;
        this->back_index = right.back_index;
        return *this;
    }

    bool fifo::is_empty(){ //Return true if the fifo is empty and false if it is not
        if(back_index == front_index) {
            return true;
        }
        else {
            return false;
        }
    }

    int fifo::size(){ //Return the size of the fifo
        return back_index - front_index;
    }

    std::string fifo::top(){ //Return the front string of the fifo.
        if(is_empty()) {
            throw 1;
        }
        else {
            return fifo_storage[front_index]; //this should return a reference, so is dereference neecessary?
        }
    }

    void fifo::enqueue(std::string input) { //Add input string to the back of the fifo
        back_index++;
        fifo_storage.append(input);
    }

    void fifo::dequeue() { //Remove the front string from the fifo
        if(is_empty()) {
            throw 1;
        }
        else {
            front_index++;
        }
    }
}


#include "../inc/stringVector.h"

namespace lab2 {
    stringVector::stringVector() { //default ctor
        data = nullptr;
        length = 0;
        allocated_length = 0;
    }

    stringVector::~stringVector() {
        //Destructor, should deallocate all memory used by object (NO MEMORY LEAKS)
        delete[] data;
        data = nullptr;
    }

    unsigned stringVector::size() const{
        //return the number of strings stored in array
        return this->length;
    }

    unsigned stringVector::capacity() const{
        //return number of strings currently allocated to be stored in array
        return this->allocated_length;
    }

    void stringVector::reserve(unsigned new_size) {
        //Allows user to choose the allocation size,
        // if it is small than current array then data should be truncated to fit
        //"truncate": create new array of new_size and transfer data over
        allocated_length = new_size;

        if(new_size < length) {
            std::string *new_array = new std::string[new_size];

            for(int i = 0; i < new_size; i++) {
                new_array[i] = data[i];           //copy element from old array one by one
            }

            delete[] data;
            //this->data = nullptr;                  //delete what data points to (the old array)
            this->data = new std::string[new_size];

            for(int i = 0; i < new_size; i++) {
                data[i] = new_array[i];           //copy elements back into data, now with the appropriate size
            }

            delete[] new_array;

            length = new_size;
        }
        if(new_size > length) {
            std::string *new_array = new std::string[new_size];

            for(int i = 0; i < length; i++) {
                new_array[i] = data[i];           //copy element from old array one by one
            }

            delete[] data;
            this->data = new std::string[new_size];

            for(int i = 0; i < length; i++) {
                data[i] = new_array[i];           //copy elements back into data, now with the appropriate size
            }

            delete[] new_array;
        }


    }

    bool stringVector::empty() const{
        //returns true IFF the array is empty
        return this->length == 0;
    }

    void stringVector::append(std::string new_data) {
        //append data to end of array
        //if capacity is zero set it to 1 (and create the array!), otherwise double array capacity if this is over capacity
        if (allocated_length == 0) {
            allocated_length = 1;
            this->data = new std::string[allocated_length];
            data[0] = new_data; //first element of array gets string
            length++;           //array now has one element
        }
        else if (length == allocated_length) {
            allocated_length = allocated_length*2; //double capacity of array
            std::string *new_array = new std::string[allocated_length];

            //must copy contents of original array to new array
            for(int i = 0; i < length; i++) {
                new_array[i] = data[i];           //copy element from old array one by one
            }

            delete[] data;
            this->data = new std::string[allocated_length];

            for(int i = 0; i < length; i++) {
                data[i] = new_array[i];    //copy elements back into data, now with the appropriate size
            }

            delete[] new_array;

            data[length] = new_data;
            length++;
        }
        else{
            data[length] = new_data;
            length++;
        }


    }

    void stringVector::swap(unsigned pos1, unsigned pos2) {
        //swap the string in position1 (pos1) of the array with the string in position2,
        // throw an exception if either position is out of bounds

        if(pos1 < 0 || pos1 > length - 1 || pos2 < 0 || pos2 > length - 1) {
            throw 1;
        }

        std::string p1String = data[pos1];
        data[pos1] = data[pos2];
        data[pos2] = p1String;
    }

    stringVector &stringVector::operator=(stringVector const &rhs) {
        //Copies RHS to object calling the function (this should be a hard COPY,
        // creating a separate object with same values

        std::string *new_array = new std::string[rhs.allocated_length];

        for(int i = 0; i < rhs.length; i++) {
            new_array[i] = rhs.data[i];           //copy element from old array one by one
        }

        delete[] this->data;
        //this->data = nullptr;                  //delete what data points to (the old array)
        this->data = new std::string[rhs.allocated_length];

        for(int i = 0; i < rhs.length; i++) {
            this->data[i] = new_array[i];        //copy elements back into data, now with the appropriate size
        }

        this->allocated_length = rhs.allocated_length;
        this->length = rhs.length;

        return *this;
    }

    std::string &stringVector::operator[](unsigned position) {
        //return a reference to the string at this position, throw an exception if out of bounds

        if(this->data == nullptr || position < 0 || position > this->length - 1) {
            throw 1;
        }

        return this->data[position];
    }

    void stringVector::sort() {
        //use the bubble sort function discussed in lab to sort the vector like a dictionary
        //(lower letters and less letters first)
        std::string temp;

        for(int i = 0; i < length - 1; i++) {
            for(int j = i + 1; j < length; j++) {
                if(data[i] > data[j]) {
                    temp = data[i];
                    data[i] = data[j];
                    data[j] = temp;
                }
            }

        }
    }

    void stringVector::pop_back() {
        length--;
    }
}

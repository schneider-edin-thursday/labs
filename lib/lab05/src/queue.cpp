#include "queue.h"
namespace lab5{
    queue::queue() {
    }

    queue::queue(std::string &data) {
        storage_structure.append(data);
    }

    queue::queue(const queue &original) {
        this->storage_structure = original.storage_structure;
    }

    queue::~queue() {
        storage_structure.~linked_list();
    }

    queue &queue::operator=(const queue &RHS) {
        this->storage_structure = RHS.storage_structure;
        return *this;
    }

    bool queue::isEmpty() const {
        return storage_structure.isEmpty();
    }

    unsigned queue::queueSize() const {
        return storage_structure.listSize();
    }

    std::string queue::top() const {
        return storage_structure.get_value_at(0);
    }

    void queue::enqueue(const std::string &data) {
        storage_structure.append(data);
    }

    void queue::dequeue() {
        storage_structure.remove(0);
    }

    std::ostream& operator<<(std::ostream &stream, queue &RHS) {
        return stream;
    }

    std::istream& operator>>(std::istream &stream, queue &RHS) {
        return stream;
    }
}
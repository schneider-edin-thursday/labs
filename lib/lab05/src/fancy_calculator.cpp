#include "fancy_calculator.h"
#include "stack.h"
#include "queue.h"

namespace lab5{
    // AUXILIARY FUNCTIONS

    bool is_number(std::string input_string) {
        char ch = input_string[0]; //string should only have one char in it, in the first pos
        return (ch >= '0' && ch <= '9');
    }

    bool is_operator(std::string input_string) {
        char ch = input_string[0];
        if (ch == '+' || ch == '-' || ch == '*' || ch == '/')
            return true;
        return false;
    }

    bool is_left_paren(std::string input_string) {
        char ch = input_string[0];
        if (ch == '(')
            return true;
        return false;
    }

    bool is_right_paren(std::string input_string) {
        char ch = input_string[0];
        if (ch == ')')
            return true;
        return false;
    }

    int priority(std::string input_string) {
        char op = input_string[0];
        switch (op) {
            case '^': return 3;
            case '*':
            case '/':
            case '%': return 2;
            case '+':
            case '-': return 1;
            default : return 0;
        }
    }

    int get_number(std::string input_string) {
        int num = stoi(input_string);
        return num;
    }

    void calculator::parse_to_infix(std::string &input_expression) { //PRIVATE function used for converting input string into infix notation

        lab1::expressionstream exp = lab1::expressionstream(input_expression);
        for(int i = 0; i < (input_expression.length() - (input_expression.length() / 2)); i++) { //this gives the actual token length without whitespace, but only works
            //when input is given in the same format as in lab04_tests
            infix_expression.enqueue(exp.get_next_token()); //pushes tokens one by one into infix, should skip spaces
        }
    }

    void calculator::convert_to_postfix(lab5::queue infix_expression) { //PRIVATE function used for converting infix FIFO to postfix

        lab5::stack operatorStack;

        while(!infix_expression.isEmpty()) {

            if (is_number(infix_expression.top())) {
                postfix_expression.enqueue(infix_expression.top());
            }
            else if (is_left_paren(infix_expression.top())) {
                operatorStack.push(infix_expression.top());
            }
            else if (is_operator(infix_expression.top())) {

                if (operatorStack.isEmpty())
                    operatorStack.push(infix_expression.top());

                else {

                    while (!operatorStack.isEmpty() && !is_left_paren(operatorStack.top()) &&
                           priority(operatorStack.top()) >= priority(infix_expression.top())) {
                        postfix_expression.enqueue(operatorStack.top());
                        operatorStack.pop();
                    }
                    operatorStack.push(infix_expression.top());
                }
            }
            else if (is_right_paren(infix_expression.top())) {

                while (!is_left_paren(operatorStack.top())) {
                    postfix_expression.enqueue(operatorStack.top());
                    operatorStack.pop();
                }
                operatorStack.pop(); //this should discard the left paren on the stack, and the right paren should be discarded by the following infix dequeue
            }

            infix_expression.dequeue();
        }

        //when the entire infix expression has been scanned, pop operators from the stack and place them in the postfix exp until the stack is empty

        while(!operatorStack.isEmpty()) {
            postfix_expression.enqueue(operatorStack.top());
            operatorStack.pop();
        }

    }

    calculator::calculator() { //Default constructor

    }

    calculator::calculator(std::string &input_expression) { // Constructor that converts input_expression to infix and postfix upon creation
        parse_to_infix(input_expression);
        convert_to_postfix(infix_expression);
    }

    std::istream &operator>>(std::istream &stream, calculator &RHS) { //Store the infix and postfix expression in calculator
        return stream;
    }

    int lab5::calculator::calculate() { //Return the calculation of the postfix expression
        lab5::stack result;
        //parse expression from left to right
        while(!postfix_expression.isEmpty()) {

            //when an operand is encountered, it gets pushed onto the stack
            if (is_number(postfix_expression.top())) {
                result.push(postfix_expression.top());
            }
                //when an operator is encountnered, pop stack twice, perform operation, then push result back onto stack
            else if (is_operator(postfix_expression.top())) {

                int op2 = get_number(result.top());
                result.pop();
                int op1 = get_number(result.top());
                result.pop();

                if (postfix_expression.top() == "^") {
                    std::string s = std::to_string(op1 ^ op2);
                    result.push(s);
                }
                else if (postfix_expression.top() == "*") {
                    std::string s = std::to_string(op1 * op2); //is this legal, or do i need to do the calculation first?
                    result.push(s);
                }
                else if (postfix_expression.top() == "/") {
                    std::string s = std::to_string(op1 / op2);
                    result.push(s);
                }
                else if (postfix_expression.top() == "%") {
                    std::string s = std::to_string(op1 % op2);
                    result.push(s);
                }
                else if (postfix_expression.top() == "+") {
                    std::string s = std::to_string(op1 + op2);
                    result.push(s);
                }
                else if (postfix_expression.top() == "-") {
                    std::string s = std::to_string(op1 - op2);
                    result.push(s);
                }
            }
            postfix_expression.dequeue();
        }
        //when exp is parsed, the single value left on stack is the result
        return get_number(result.top());
    }

    std::ostream &operator<<(std::ostream &stream, calculator &RHS) { //Stream out overload. Should return in the format "Infix: #,#,#,#\nPostfix: #,#,#,#"
        lab5::queue infix_copy;
        infix_copy.operator=(RHS.infix_expression);

        lab5::queue postfix_copy;
        postfix_copy.operator=(RHS.postfix_expression);

        std::string infix = "Infix: ";
        std::string postfix = "Postfix: ";
        std::string comma = ",";
        std::string newline = "\n";

        stream << infix;

        while(infix_copy.queueSize() > 0) {
            if(infix_copy.top() == "") {
                infix_copy.dequeue();

                if(infix_copy.isEmpty()) {
                    stream << newline;
                }
            }
            else {
                stream << infix_copy.top();
                infix_copy.dequeue();

                if(!infix_copy.isEmpty() && infix_copy.top() != "") {
                    stream << comma;
                }
                else if (infix_copy.isEmpty()){
                    stream << newline;
                }
            }

        }

        stream << postfix;

        while(postfix_copy.queueSize() > 0) {
            stream << postfix_copy.top();
            postfix_copy.dequeue();

            if(!postfix_copy.isEmpty()) {
                stream << comma;
            }
        }

        return stream;
    }


}


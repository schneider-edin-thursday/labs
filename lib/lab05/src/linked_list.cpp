#include <linked_list.h>
namespace lab5 {
    linked_list::linked_list() { //default ctor
        head = nullptr;
        tail = nullptr;
        //I think the way we're meant to implement it is that head and tail ARE nodes, and therefore contain data, instead of just being pointers.
    }

    linked_list::linked_list(std::string &data) { //same as default ctor but creates list with one data item in it
        head = nullptr;
        tail = nullptr;
        append(data);
    }

    linked_list::linked_list(const linked_list &original) { //copy constructor
        if(original.head == nullptr) {
            this->head = nullptr;
            this->tail = nullptr;
        }
        else{
            node *new_head = new node(original.head->data); // copy head first, can't just point to it
            this->head = new_head; //do we use 'this'?
            this->tail = new_head; //this will be updated as we append copies of original's nodes onto this copy

            node *p = original.head; //traversal pointer for original
            p = p->next; //remember we already put a copy of the original head into the copy list

            while(p!=nullptr) {
                this->append(p->data);
                p = p->next;
            }
        }

    }

    linked_list::~linked_list() { //destructor
        node *p = head;
        node *q = nullptr;

        while(p!=nullptr) {
            q = p->next;
            free(p);
            p = q;
        }

    }

    //AUXILIARY FUNCTIONS

    node& get_node(node* head, unsigned location) { //returns reference to node at a given location
        node *p = head;

        if(location == 0) {
            return *p;
        }
        else {
            for(int i = 0; i < location; i++) {
                p = p->next;
            }
            return *p;
        }

    }

    linked_list &lab5::linked_list::operator=(const linked_list &RHS) {
        node *p = RHS.head;

        while(p!=nullptr) {
            this->append(p->data); //this creates a new node with the data of a node in RHS and puts it at the end of this
            p = p->next;
        }

        return *this;
    }


    bool linked_list::isEmpty() const { //returns true if list is empty (head->next == nullptr?)

        if(head == nullptr)//maybe just head->next == nullptr
            return true;
        else
            return false;
    }

    unsigned linked_list::listSize() const { // returns size, nodes must be counted!

        node *p = head;

        int count = 0;
        while (p != nullptr) {
            ++count;
            p = p->next;

        }

        return count;
    }

    void linked_list::insert(const std::string input, unsigned int location) { //default location is 0 if unspecified

        if(location > this->listSize()) {
            throw 1;
        }

        node *new_node = new node(input);
        node *p = head;

        if(p != nullptr) {

            if(location == 0) {
                new_node->next = p;
                head = new_node;
            }
            else {
                for(int i = 0; i < location - 1; i++) { //ends when p->next is pointing to the location you want to insert the node
                    p = p->next;
                }

                new_node->next = p->next;
                p->next = new_node;

                if(new_node->next == nullptr) { //condition could also be if(location == this->listSize())
                    tail = new_node;
                }
            }

        }
        else{
            head = new_node;
            tail = new_node;
        }

    }

    void linked_list::append(const std::string input) { //create a new node and put it at the end/priority_tail of the linked list
        node *new_node = new node(input);
        //new_node->data = input;
        new_node->next = nullptr;

        if(head == nullptr) {
            head = new_node;
            tail = new_node;
        }
        else {
            tail->next = new_node;
            tail = tail->next;
        }
    }

    void linked_list::remove(unsigned location) { //Remove the node at the given location

        if(location > this->listSize() - 1 || head == nullptr) {
            throw 1;
        }

        node *p = head;

        if(location == 0) {
            head = p->next;
            p = nullptr;
        }
        else {
            for(int i = 0; i < location - 1; i++) { //ends when p->next is pointing to the node you want to remove
                p = p->next;
            }
            node *q = p->next;
            p->next = q->next;
            q = nullptr;

            if(p->next == nullptr) { //this would mean p has become the last node as a result of the operation
                tail = p;
            }
        }



    }

    std::ostream& operator<<(std::ostream &stream, linked_list &RHS) {
        node *p = RHS.head;

        while(p!=nullptr) {
            stream << p->data << " -> ";
            p = p->next;
        }
        stream << "NULL";

        return stream;
    }

    std::istream& operator>>(std::istream &stream, linked_list &RHS) {
        std::string s;
        stream >> s;

        RHS.append(s);

        return stream;
    }

    void linked_list::sort() {

        for(int i = 0; i < listSize() - 1; i++) {
            int k = i;
            std::string s = get_value_at(i); // s = "smallest" value found so far. assume first value in list is the smallest
            for(int j = i+1; j < listSize(); j++)  //second loop comparing all remaining elements of list to smallest element found thus far
                if(get_value_at(j) < s) {
                    k = j; //saves value for swapping
                    s = get_value_at(j); //new "smallest" value
            }
            get_node(head,k).data = get_value_at(i); //"smallest" and comparing node swapped
            get_node(head,i).data = s;
        }
    }

    std::string linked_list::get_value_at(unsigned location) const {

        if(location > this->listSize() - 1) { // unlike with insert() we don't want to allow the location to be one more than the tail index
            throw 1;
        }

        node *p = head;

        for(int i = 0; i < location; i++) { //ends when p is at the desired note
            p = p->next;
        }

        return p->data;
    }

}
#include "../inc/hash_table.h"

namespace lab8{
    unsigned hash_table::hash_1(std::string to_hash) {
        // DJB2 Hashing Algorithm
        unsigned int hash = 5381;
        for(char c: to_hash){
            hash = ((hash << 5) + hash) + c;
        }
        return hash;
    }

    unsigned hash_table::hash_2(std::string to_hash) {
        // BKDR Hashing Algorithm
        unsigned int seed = 131;
        unsigned int hash = 0;
        for(char c: to_hash){
            hash = (hash * seed) + c;
        }
        return hash;
    }

    void hash_table::expand() {

        int i = 0;
        while(PRIMES[i] <= max_size){
            i++;
        }

        hash_table new_table = hash_table(this->probing);
        new_table.hash_table_array = new key_value[PRIMES[i]];
        new_table.max_size = PRIMES[i];

        //must REHASH values into larger hash table since probing is based on max_size
        for(int j = 0; j < this->max_size; j++) { //max_size has not been updated yet
            if(this->hash_table_array[j].key != "")
                new_table.insert(this->hash_table_array[j].key, this->hash_table_array[j].value);
        }

        delete[] this->hash_table_array;
        this->max_size = PRIMES[i];
        this->hash_table_array = new key_value[this->max_size];

        for(int i = 0; i < max_size; i++) {
            this->hash_table_array[i] = new_table.hash_table_array[i];    //copy elements back into data, now with the appropriate size
        }

    }

    hash_table::hash_table(char type) {
        /*
         * Define the probing technique
         * 'l': Linear probing
         *          hash_1() + attempt
         * 'q': Quadratic probing
         *          hash_1() + attempt^2
         * 'd': Double Probing
         *          hash_1() + attempt * hash_2()
         *
         * Create a hash table with an initial size of 100
         */

        probing = type;
        max_size = 100;
        hash_table_array = new key_value[max_size];
        current_size = 0;


    }

    hash_table::~hash_table() {
        delete[] hash_table_array;
        hash_table_array = nullptr;
    }

    float percent_full(int size, int max_size){
        float s = size;
        float m = max_size;

        return (s/m) * 100.0;
    }

    bool hash_table::insert(std::string key, int value) {
        // Insert a key according to the defined probing technique
        // If you run into an issue where you get an infinite collision loop,
        //   figure out a way to get out of the loop.

        if(in_table(key))
            return false;

        key_value new_element = {key, value};
        int n = hash_1(key);

        if(hash_table_array[n % max_size].key == "" || hash_table_array == nullptr){  // an uninitialized string means the spot is free
            hash_table_array[n % max_size] = new_element;
            current_size++;
            return true;
        }
        else{
            int attempt = 1;

            if(probing == 'l'){
                std::string debug;
                while(hash_table_array[(n + attempt) % max_size].key != "" && attempt < current_size){ // need to handle potential infinite collision loop here
                    debug = hash_table_array[(n + attempt) % max_size].key;
                    attempt++;
                }
                if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                    for(int i = (n + attempt) % max_size; i <= max_size; i++){
                        if(i == max_size)
                            i = 0;
                        if(hash_table_array[i].key == ""){
                            hash_table_array[i] = new_element;
                            current_size++;
                            if(percent_full(current_size, max_size) >= 70.0)
                                expand();
                            return true;
                        }
                    }
                }
                hash_table_array[(n + attempt) % max_size] = new_element;
                current_size++;
                if(percent_full(current_size, max_size) >= 70.0)
                    expand();
                return true;
            }
            if(probing == 'q'){
                while(hash_table_array[(n + attempt^2) % max_size].key != "" && attempt < current_size){
                    attempt++;
                }
                if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                    for(int i = (n + attempt) % max_size; i <= max_size; i++){
                        if(i == max_size)
                            i = 0;
                        if(hash_table_array[i].key == ""){
                            hash_table_array[i] = new_element;
                            current_size++;
                            if(percent_full(current_size, max_size) >= 70.0)
                                expand();
                            return true;
                        }
                    }
                }
                hash_table_array[(n + attempt^2) % max_size] = new_element;
                current_size++;
                if(percent_full(current_size, max_size) >= 70.0)
                    expand();
                return true;
            }
            if(probing == 'd'){
                int m = hash_2(key);
                while(hash_table_array[(n + attempt * m) % max_size].key != "" && attempt < current_size){
                    attempt++;
                }
                if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                    for(int i = (n + attempt) % max_size; i <= max_size; i++){
                        if(i == max_size)
                            i = 0;
                        if(hash_table_array[i].key == ""){
                            hash_table_array[i] = new_element;
                            current_size++;
                            if(percent_full(current_size, max_size) >= 70.0)
                                expand();
                            return true;
                        }
                    }
                }
                hash_table_array[(n + attempt * m) % max_size] = new_element;
                current_size++;
                if(percent_full(current_size, max_size) >= 70.0)
                    expand();
                return true;
            }
        }
    }

    bool hash_table::in_table(std::string key){
        // Checks to see if that key is in the table.
        // Use the specified probing technique
        // Keep collisions in mind

        if(hash_table_array == nullptr || current_size == 0)
            return false;

        int n = hash_1(key);

       if(hash_table_array[n % max_size].key == key)
            return true;
       else{
           int attempt = 1;

           if(probing == 'l'){
               std::string debug;
               while(hash_table_array[(n + attempt) % max_size].key != key && attempt < current_size){
                   debug = hash_table_array[(n + attempt) % max_size].key;
                   attempt++;
               }
               if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                   for(int i = (n + attempt) % max_size; i <= max_size; ++i){
                       if(i == max_size)
                           i = 0;
                       if(i == ((n + attempt) % max_size)) //this would mean that we've looped all the way back around
                           return false;
                       if(hash_table_array[i].key == key)
                           return true;
                   }
               }
               return true;
           }
           if(probing == 'q'){
               while(hash_table_array[(n + attempt^2) % max_size].key != key && attempt < current_size){
                   attempt++;
               }
               if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                   for(int i = (n + attempt) % max_size; i <= max_size; ++i){
                       if(i == max_size)
                           i = 0;
                       if(i == ((n + attempt) % max_size)) //this would mean that we've looped all the way back around
                           return false;
                       if(hash_table_array[i].key == key)
                           return true;
                   }
               }
               return true;
           }
           if(probing == 'd'){
               int m = hash_2(key);
               while(hash_table_array[(n + attempt * m) % max_size].key != key && attempt < current_size){
                   attempt++;
               }
               if(attempt >= current_size){ // if an infinite hasing loop encountered, simply insert element in next available spot, looping back around if necessary
                   for(int i = (n + attempt) % max_size; i <= max_size; ++i){
                       if(i == max_size)
                           i = 0;
                       if(i == ((n + attempt) % max_size)) //this would mean that we've looped all the way back around
                           return false;
                       if(hash_table_array[i].key == key)
                           return true;
                   }
               }
               return true;
           }
       }
        return false;
    }

    int hash_table::get(std::string key) {
        // Get the int value from the node that has key
        // Use the specified probing technique

        if(!in_table(key))
            throw 1;

        int n = hash_1(key);

        if(hash_table_array[n % max_size].key == key)
            return hash_table_array[n % max_size].value;
        else{
            int attempt = 1;

            if(probing == 'l'){
                while(hash_table_array[(n + attempt) % max_size].key != key){
                    attempt++;
                }
                return hash_table_array[(n + attempt) % max_size].value;
            }
            if(probing == 'q'){
                while(hash_table_array[(n + attempt^2) % max_size].key != key){
                    attempt++;
                }
                return hash_table_array[(n + attempt^2) % max_size].value;
            }
            if(probing == 'd'){
                int m = hash_2(key);
                while(hash_table_array[(n + attempt * m) % max_size].key != key){;
                    attempt++;
                }
                return hash_table_array[(n + attempt * m) % max_size].value;
            }
        }
        return 0;
    }

    void hash_table::update(std::string key, int value){
        // Update a key in the hash table.
        // Keep collisions in mind
        // Use the specified probing technique

        if(!in_table(key))
            insert(key, value);
        else{
            int n = hash_1(key);

            if(hash_table_array[n % max_size].key == key)
                hash_table_array[n % max_size].value = value;
            else{
                int attempt = 1;

                if(probing == 'l'){
                    while(hash_table_array[(n + attempt) % max_size].key != key){
                        attempt++;
                    }
                    hash_table_array[(n + attempt) % max_size].value = value;
                }
                if(probing == 'q'){
                    while(hash_table_array[(n + attempt^2) % max_size].key != key){
                        attempt++;
                    }
                    hash_table_array[(n + attempt^2) % max_size].value = value;
                }
                if(probing == 'd'){
                    int m = hash_2(key);
                    while(hash_table_array[(n + attempt * m) % max_size].key != key){;
                        attempt++;
                    }
                    hash_table_array[(n + attempt * m) % max_size].value = value;
                }
            }
        }
    }

    void hash_table::remove(std::string key){
        // Remove an item from the hash table. Keep collisions in mind
        // Use the specified probing technique
        // If the item isn't in the table, do nothing.

        if(!in_table(key))
            return;

        int n = hash_1(key);

        if(hash_table_array[n % max_size].key == key){
            hash_table_array[n % max_size].value = 0;
            hash_table_array[n % max_size].key = "";
        }
        else{
            int attempt = 1;

            if(probing == 'l'){
                while(hash_table_array[(n + attempt) % max_size].key != key){
                    attempt++;
                }
                hash_table_array[(n + attempt) % max_size].value = 0;
                hash_table_array[(n + attempt) % max_size].key = "";
            }
            if(probing == 'q'){
                while(hash_table_array[(n + attempt^2) % max_size].key != key){
                    attempt++;
                }
                hash_table_array[(n + attempt^2) % max_size].value = 0;
                hash_table_array[(n + attempt^2) % max_size].key = "";
            }
            if(probing == 'd'){
                int m = hash_2(key);
                while(hash_table_array[(n + attempt * m) % max_size].key != key){;
                    attempt++;
                }
                hash_table_array[(n + attempt * m) % max_size].value = 0;
                hash_table_array[(n + attempt * m) % max_size].key = "";
            }
        }
    }

    std::string hash_table::to_string(){
        // Run through the entire array and create a string following this format. The {} brackets aren't part of the return
        // [{array location}]{key_value.key}:{key_value.int}
        std::string output;
        for(int i = 0; i < max_size; i++){
            output += "[";
            output += i;
            output += "]";
            output += hash_table_array[i].key;
            output += ":";
            output += hash_table_array[i].value;
            output += " ";
        }
        return std::string();
    }
}

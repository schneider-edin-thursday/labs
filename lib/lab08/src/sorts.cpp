#include "doubly_linked_list.h"

namespace lab6{
    // Auxiliary Functions
    doubly_linked_list recursive_merge_sort(doubly_linked_list to_sort);
    doubly_linked_list merge(doubly_linked_list left, doubly_linked_list right);

    int min(int x, int y){
        return (x < y)? x : y;
    }

    unsigned partition(doubly_linked_list& to_sort, int low, int high){

        int pivot = to_sort.get_data(high);
        int i = low - 1;

        for(int j = low; j <= high - 1; j++){
            if(to_sort.get_data(j) <= pivot){
                i++;
                to_sort.swap(i, j);
            }
        }
        to_sort.swap(i + 1, high);
        return i + 1;
    }

    void quicksort(doubly_linked_list& to_sort, int low, int high){
        if(low < high){
            int partition_index = partition(to_sort, low, high);

            quicksort(to_sort, low, partition_index - 1);
            quicksort(to_sort, partition_index + 1, high);
        }
    }

    void mergesort(doubly_linked_list& to_sort){
        to_sort = recursive_merge_sort(to_sort);
    }

    doubly_linked_list recursive_merge_sort(doubly_linked_list to_sort){
        if(to_sort.size() <= 1)
            return to_sort;

        doubly_linked_list left;
        doubly_linked_list right;

        for(int i = 0; i < to_sort.size(); i++){
            if(i < to_sort.size() / 2)
                left.append(to_sort.get_data(i));
            else
                right.append(to_sort.get_data(i));
        }

        left = recursive_merge_sort(left);
        right = recursive_merge_sort(right);

        return merge(left, right);
    }

    //Used for the merging the two lists
    doubly_linked_list merge(doubly_linked_list left, doubly_linked_list right){
        doubly_linked_list result;

        while(!left.is_empty() && !right.is_empty()){
            if(left.get_data(0) <= right.get_data(0)){
                result.append(left.get_data(0));
                left.remove(0);
            }
            else{
                result.append(right.get_data(0));
                right.remove(0);
            }
        }

        while(!left.is_empty()){
            result.append(left.get_data(0));
            left.remove(0);
        }
        while(!right.is_empty()){
            result.append(right.get_data(0));
            right.remove(0);
        }

        return result;
    }
}
#include "../inc/doubly_linked_list.h"

namespace lab6{
    doubly_linked_list::doubly_linked_list() {
        head = nullptr;
        tail = nullptr;
    }

    doubly_linked_list::doubly_linked_list(int input) {
        head = nullptr;
        tail = nullptr;
        append(input);
    }

    doubly_linked_list::doubly_linked_list(std::vector<int> vector_input) {
        head = nullptr;
        tail = nullptr;
        for(int i = 0; i < vector_input.size(); i++) {
            append(vector_input[i]);
        }
    }

    doubly_linked_list::doubly_linked_list(const doubly_linked_list &original) { //copy ctor
        if(original.head == nullptr) {
            this->head = nullptr;
            this->tail = nullptr;
        }
        else{
            node *new_head = new node(original.head->get_data()); // copy head first, can't just point to it, I believe by default next and prev are set to NULL
            this->head = new_head; //do we use 'this'?
            this->tail = new_head; //this will be updated as we append copies of original's nodes onto this copy

            node *p = original.head; //traversal pointer for original
            p = p->next; //remember we already put a copy of the original head into the copy list

            while(p!=nullptr) {
                this->append(p->get_data());
                p = p->next;
            }
        }
    }

    doubly_linked_list::~doubly_linked_list() {
        node *p = head;

        while(head!=nullptr) {
            p = head;
            head = head->next;
            delete p;
        }
    }

    int doubly_linked_list::get_data(unsigned position) {

        if(position > this->size() - 1) { // unlike with insert() we don't want to allow the location to be one more than the tail index
            throw 1;
        }

        node *p = head;

        for(int i = 0; i < position; i++) { //ends when p is at the desired node
            p = p->next;
        }

        return p->get_data();
    }

    std::vector<int> doubly_linked_list::get_set(unsigned position_from, unsigned position_to) {

        if(head == nullptr || position_from < 0 || position_to > this->size() - 1) {
            throw 1;
        }

        std::vector<int> set;
        node *p = head;

        for(int i = 0; i < position_from; i++) { //ends when p is at the desired node
            p = p->next;
        }

        for(int i = position_from; i <= position_to; i++) {
            set.push_back(p->get_data());
            p = p->next;
        }

        return set;
    }

    unsigned doubly_linked_list::size() {

        node *p = head;

        int count = 0;
        while (p != nullptr) {
            ++count;
            p = p->next;

        }

        return count;
    }

    bool doubly_linked_list::is_empty() {
        if(head == nullptr)
            return true;
        else
            return false;
    }

    void doubly_linked_list::append(int input) {

        node *new_node = new node(input);

        if(head == nullptr) {
            head = new_node;
            tail = new_node;
        }
        else {
            tail->next = new_node;
            new_node->prev = tail;
            tail = tail->next;
        }

    }

    void doubly_linked_list::insert(int input, unsigned int location) {

        if(location > this->size()) {
            throw 1;
        }

        node *new_node = new node(input);
        node *p = head;

        if(p != nullptr) {

            if(location == 0) {
                new_node->next = p;
                p->prev = new_node;
                head = new_node;
            }
            else {
                for(int i = 0; i < location; i++) { //ends when p is pointing to the location you want to insert the node
                    p = p->next;
                }

                if(p == nullptr) {
                    tail->next = new_node;
                    new_node->prev = tail;
                }
                else {
                    new_node->next = p;
                    new_node->prev = p->prev;
                    p->prev->next = new_node;
                    p->prev = new_node;
                    p = new_node;
                }

                if(new_node->next == nullptr) { //condition could also be if(location == this->listSize())
                    tail = new_node;
                }
            }

        }
        else{
            head = new_node;
            tail = new_node;
        }
    }

    void doubly_linked_list::remove(unsigned location) {

        if(location > this->size() - 1 || head == nullptr) {
            throw 1;
        }

        node *p = head;

        if(location == 0) {
            head = p->next;

            if(head != nullptr) { //if there are still items in list after removal
                head->prev = nullptr;
            }

            p = nullptr;
        }
        else {
            for(int i = 0; i < location; i++) { //ends when p is pointing to the node you want to remove
                p = p->next;
            }

            node *q = p->prev;
            node *r = p->next;

            q->next = r;

            if(r != nullptr) {
                r->prev = q;
            }
            else { //this would mean we are removing the last node in the list
                q->next = nullptr;
                tail =q;
                q = nullptr;
            }
        }
    }

    doubly_linked_list doubly_linked_list::split(unsigned position) {

        if(head == nullptr || position < 0 || position > size() - 1) {
            throw 1;
        }

        node *p = head;

        for(int i = 0; i < position; i++) { //ends when p is pointing to the node that begins the split
            p = p->next;
        }

        std::vector<int> split = get_set(position, size() - 1);
        doubly_linked_list new_list = doubly_linked_list(split);

        int original_size = size();

        for(int i = position; i < original_size; i++) {
            this->remove(position);
        }

        return new_list;

    }

    doubly_linked_list doubly_linked_list::split_set(unsigned position_1, unsigned position_2) { // make a list out of position range, append remainder of list to the elements preceding the range

        if (head == nullptr || position_1 < 0 || position_1 > size() - 1 ||
            position_1 > position_2 || position_2 > size() - 1) {
            throw 1;
        }

        node *p = head;

        for(int i = 0; i < position_1; i++) { //ends when p is pointing to the node that begins the split
            p = p->next;
        }

        std::vector<int> split = get_set(position_1, position_2);
        doubly_linked_list new_list = doubly_linked_list(split);

        for(int i = position_1; i <= position_2; i++) { //watch for edge cases here
            this->remove(position_1);
        }

        return new_list;
    }

    void doubly_linked_list::swap(unsigned position_1, unsigned position_2) {

        if(head == nullptr || position_1 < 0 || position_1 > size() - 1 || position_2 < 0 || position_2 > size() - 1 ) {
            throw 1;
        }

        node *p = head;

        for(int i = 0; i < position_1; i++) {
            p = p->next;
        }

        node *q = head;

        for(int i = 0; i < position_2; i++) {
            q = q->next;
        }

        remove(position_1);
        insert(q->get_data(), position_1);
        remove(position_2);
        insert(p->get_data(), position_2);


    }

    void doubly_linked_list::swap_set(unsigned location_1_start, unsigned location_1_end, unsigned location_2_start,
                                      unsigned location_2_end) {

        if (head == nullptr || location_1_start > size() - 1  || location_1_end > size() - 1 || location_2_start > size() - 1 || location_2_end > size() - 1 ||
            location_1_start > location_1_end || location_2_start > location_2_end) {
            throw 1;
        }

        std::vector<int> set_1 = get_set(location_1_start, location_1_end); // create new list out of first set of elements to be swapped
        doubly_linked_list new_list_1 = doubly_linked_list(set_1);

        std::vector<int> set_2 = get_set(location_2_start, location_2_end); // create new list out of second set of elements to be swapped
        doubly_linked_list new_list_2 = doubly_linked_list(set_2);

        for(int i = location_1_start; i <= location_1_end; i++) { // remove first swap set from original list
            this->remove(location_1_start);
        }

        int offset = location_1_end - location_1_start + 1; // when you remove elements the locations are no longer accurate, must update

        for(int i = location_2_start; i <= location_2_end; i++) { // remove second swap set from original list
            this->remove(location_2_start - offset);
        }

        node *p = new_list_1.head;
        node *q = new_list_2.head;

        int incr = location_1_start;

        while(q != nullptr) { // insert each element from first swap set where the second set once was, being sure to increment the insertion position so it's in the right order
            this->insert(q->get_data(), incr);
            q = q->next;
            incr++;
        }

        incr = location_2_start;

        while(p != nullptr) { // insert each element from first swap set where the second set once was, being sure to increment the insertion position so it's in the right order
            this->insert(p->get_data(), incr);
            p = p->next;
            incr++;
        }


    }

    void doubly_linked_list::sort() {
        // Implement Insertion Sort
        for(int i = 1; i < size(); i++) {
            int temp = get_data(i);
            int j = i - 1;
            while(j >= 0 && temp < get_data(j)) {
                remove(j+1);
                insert(get_data(j), j+1);
                j--;
            }
            remove(j+1);
            insert(temp, j+1);
        }
    }

    doubly_linked_list doubly_linked_list::operator+(const doubly_linked_list &rhs) const {

        doubly_linked_list new_list = doubly_linked_list(*this);

        node *p = rhs.head;

        while(p!=nullptr) {
            new_list.append(p->get_data());
            p = p->next;
        }

        return new_list;
    }

    doubly_linked_list& doubly_linked_list::operator=(const doubly_linked_list &rhs) {

        while(this->head){
            this->remove(0);
        }

        node *p = rhs.head;

        while(p!=nullptr) {
            this->append(p->get_data()); //this creates a new node with the data of a node in RHS and puts it at the end of this
            p = p->next;
        }

        return *this;
    }

    doubly_linked_list& doubly_linked_list::operator+=(const doubly_linked_list &rhs) {

        node *p = rhs.head;

        while(p!=nullptr) {
            this->append(p->get_data());
            p = p->next;
        }

        return *this;
    }

    bool doubly_linked_list::operator==(const doubly_linked_list &rhs) {

        node *p = rhs.head;
        node *q = this->head;

        int right_size = 0;
        while (p != nullptr) {
            ++right_size;
            p = p->next;

        }

        p = rhs.head;

        if(size() != right_size)
            return false;

        while(p!=nullptr) {
            if(p->get_data() != q->get_data())
                return false;
            p = p->next;
            q = q->next;
        }

        return true;

    }

    std::ostream &operator<<(std::ostream &stream, doubly_linked_list &RHS) {
        node *p = RHS.head;

        stream << "NULL <- ";

        while(p!=nullptr) {
            if(p->next == nullptr) {
                stream << p->get_data() << " -> ";
            }
            else{
                stream << p->get_data() << " <-> ";
            }
            p = p->next;
        }
        stream << "NULL";

        return stream;
    }

    std::istream &operator>>(std::istream &stream, doubly_linked_list &RHS) {
        int s;
        stream >> s;

        RHS.append(s);

        return stream;
    }
}

